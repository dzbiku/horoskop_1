package pl.edu.pwr.kd;

//import org.hamcrest.Matcher;
import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;


import java.util.Random;

public class TestWyboru 
{

	@Test
	public void imieDzielenieModuloDwanascie()
	{
		wybierzWrozbe nowa= new wybierzWrozbe();
		String name = "Trol";
		assertThat(nowa.wybierzWrozbeZdrowie(name), is(Przepowiednie.ZDROWIE[4]));
	}
	
	@Test
	public void pracaPrzepowiedniaMiedzyZeroAjedenascie()
	{
		wybierzWrozbe nowa= new wybierzWrozbe();
		int liczbaLosowa = 4;
		assertThat(nowa.wybierzWrozbePraca(4), is(Przepowiednie.PRACA[liczbaLosowa]));
	}
	
	
	@Test
	public void miloscDzienMiesiaca()
	{
		wybierzWrozbe nowa= new wybierzWrozbe();
		int dayOf = 25;
		int numberOf = (dayOf)%12;
		assertThat(nowa.wybierzWrozbeMilosc(), is(Przepowiednie.MILOSC[numberOf]));

		
	}
}
