package pl.edu.pwr.kd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main 
{
	public static String name;
	public static String milosc;
	public static String zdrowie;
	public static String praca;
	public static int index;
	
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	public static void main(String[] args) throws IOException 
	{
		wybierzWrozbe nowa = new wybierzWrozbe();
		
		System.out.println("Podaj swoje imie...");
		name = br.readLine();
		
		milosc = nowa.wybierzWrozbeMilosc();
		zdrowie = nowa.wybierzWrozbeZdrowie(name);
		
		index = nowa.randomowaWartos();
		praca = nowa.wybierzWrozbePraca(index);
		
		System.out.println(name + " " + "Przepowiednie dla Ciebie!");
		System.out.println("Milosc: " + milosc);
		System.out.println("Zdrowie: " + zdrowie);
		System.out.println("Praca: " + praca);
	} 
}
