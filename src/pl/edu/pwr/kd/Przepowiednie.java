package pl.edu.pwr.kd;

public class Przepowiednie {

	/**
	 * Wszystkie wr�by zosta�y zaczerpni�te z horoskopu dziennego na dzie� 16.04.2016 z portalu magia.onet.pl.
	 */
	
	public static String[] MILOSC = {
			"Staraj si� nie zawie�� jako kochanek.", 
			"Zat�sknisz za gor�c� mi�o�ci�.", 
			"Unikaj spi�� z ukochan� osob�.", 
			"Post�puj spokojnie i delikatnie.", 
			"Mo�liwe trudne relacje z otoczeniem.",
			"Dobrze u�o�� si� stosunki z rodzin�.", 
			"B�d� milszy dla bliskich.", 
			"Potrzeba Ci serdecznego przyjaciela.", 
			"Znajdziesz si� w centrum zainteresowania.", 
			"B�d� bardziej pow�ci�gliwy w krytykowaniu innych.", 
			"Zawierz intuicji.",
			"Mo�liwy jest koniec samotno�ci."};
	
	public static String[] PRACA = {
			"Nie narzucaj innym swojej woli.",
			"B�dziesz po��da� mocnych wra�e�.",
			"Plany podr�y zaczn� si� urealnia�.", 
			"Znajd� troch� czasu dla siebie.", 
			"Rozs�dnie planuj wydatki.",
			"Obawy odno�nie pieni�dzy s� bezzasadne.", 
			"Zajmij si� w�asnymi sprawami.",
			"Postaraj si� bardziej wyluzowa�.",
			"Pami�taj, �e ka�da przesada bywa szkodliwa.", 
			"Nie dopuszczaj do napi�tych sytuacji.",
			"Twoje wysi�ki przynios� dobre rezultaty.", 
			"Nie podejmuj si� trudnych zada�." 
	};
	
	public static String[] ZDROWIE = {
			"Przezwyci�aj lenistwo.", 
			"Odpr�enie znajdziesz na �onie rodziny.", 
			"Wskazana ostro�no�� na drodze.", 
			"Popracuj nad swoj� kondycj� psychiczn�.", 
			"Potrzeba Ci kontaktu z przyrod�.", 
			"Postaraj si� o w�a�ciwy wypoczynek.", 
			"Panuj nad nerwami.",
			"Daj upust swojej niewykorzystanej energii.",
			"Nie rozpraszaj dobrej energii.",
			"Unikaj ci�kostrawnych potraw.", 
			"Naucz si� relaksowa�.",
			"Wprowad� do swego �ycia wi�cej rado�ci." 
	};
	
}